function [H, Xp, Yp] = geval(b, d, L)
% calculates the the heights h_i and points (X_pi, Y_pi) for i = [1, 2, 3]
P = ones(1, 3);
H = ones(1, 3);
Xp = ones(1, 3);
Yp = ones(1, 3);

for n = 1:3
    P(n) = (1/(2*b))*(b^2 + L(2*n-1)^2 - L(2*n)^2);
    if(L(2*n)^2 - P(n)^2 < 0)
        error('L%s is too short', int2str(2*n)) 
    end
    H(n) = sqrt(L(2*n-1)^2 - P(n)^2); 
end
Xp(1) = (sqrt(3)/6)*(2 * b + d - 3*P(1));
Xp(2) = (-(sqrt(3) / 6)) * (b + 2*d);
Xp(3) = (-(sqrt(3) / 6)) * (b - d - 3*P(3));
Yp(1) = (1/2) * (d + P(1));
Yp(2) = (1/2) * (b + 2*P(2));
Yp(1) = (-(1/2)) * (b + d - P(3));
end