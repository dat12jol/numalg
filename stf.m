function [ diff, Yt, Zt ] = stf(a, H, Xt, Xp, Yp)
%STF Summary of this function goes here
%   Calculates the precision of the mathematical model based on initial
%   values. Returns a big number if the value is imaginary due to physical
%   constraints. 
diff(1) =  a^2 + 2*Xt(1)*Xt(2) - (2 * Xt(1) * (Xp(1) + sqrt(3) * (Yp(1) - Yp(2)))) - 2 * Xp(2) * Xt(2) - ((sqrt(3) * Xp(1) - Yp(1) + Yp(2))^2 + (H(1)^2 + H(2)^2) - 4*Xp(1)^2 - Xp(2)^2) + 2 * sqrt((H(1)^2 - 4 * (Xt(1) - Xp(1))^2)*(H(2)^2 - (Xt(2) - Xp(2))^2));    

diff(2) = a^2 - 4 * Xt(1)* Xt(3) -2 * Xt(1) * (Xp(1) - 3 * Xp(3) + sqrt(3) * (Yp(1) - Yp(3))) - 2 * Xt(3) * (-3*Xp(1) + Xp(3) + sqrt(3)*(Yp(1)-Yp(3))) - ((sqrt(3)*(Xp(1) + Xp(3)) - Yp(1) + Yp(3))^2 + (H(1)^2 + H(3)^2) - 4*Xp(1)^2 - 4 * Xp(3)^2) + 2 * sqrt((H(1)^2 - 4*(Xt(1) - Xp(1))^2)*(H(3)^2 - 4 * (Xt(3) - Xp(3))^2));

diff(3) = a^2 + 2*Xt(2)*Xt(3) -2 * Xt(3)*(Xp(3) + sqrt(3)*(Yp(2) - Yp(3))) -2 * Xp(2) * Xt(2) - ((sqrt(3) * Xp(3) - Yp(2) + Yp(3))^2 + (H(2)^2 + H(3)^2) - Xp(2)^2 -4 * Xp(3)^2) + 2 * sqrt((H(2)^2 - (Xt(2) - Xp(2))^2) * (H(3)^2 - 4 * (Xt(3) - Xp(3))^2));

if ~isreal(diff)
   diff = [10000, 10000, 10000]; 
end
Yt(1) = sqrt(3) * Xt(1) - (sqrt(3) * Xp(1) - Yp(1));
Yt(2) = Yp(2);
Yt(3) = -sqrt(3) * Xt(3) + (sqrt(3)* Xp(3) + Yp(3));

Zt(1) = sqrt(H(1)^2 - 4 * (Xt(1) - Xp(1))^2 );
Zt(2) = sqrt(H(2)^2 - (Xt(2) - Xp(2))^2 );
Zt(3) = sqrt(H(3)^2 - 4 * (Xt(3) - Xp(3))^2 );

end

 